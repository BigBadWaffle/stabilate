import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './ui/app';
import reportWebVitals from './reportWebVitals';
import { init as initRenderer } from './renderer';
import { init as initManager } from './objects';
import { init as initMusic } from './system/music';
import random from './system/random';

window.isMobile = (
	/Mobi|Android/i.test(navigator.userAgent) ||
	(
		navigator.platform === 'MacIntel' &&
		navigator.maxTouchPoints > 1
	)
);

ReactDOM.render(
	<React.StrictMode>
		<App />
	</React.StrictMode>,
	document.getElementById('root')
);

random();
initRenderer();
initManager();
initMusic();

reportWebVitals();
