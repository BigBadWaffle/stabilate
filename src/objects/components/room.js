import { getTexture } from '../../renderer/textureCache';

import * as PIXI from 'pixi.js';

import { buildObject } from '../';

const buildSprite = (x, y, sx, sy) => {
	const texture = getTexture('images/tiles.png', sx, sy);

	let sprite = new PIXI.Sprite(texture, {
		width: 8,
		height: 8
	});
	sprite.x = x;
	sprite.y = y;

	return sprite;
};

const cpn = (object, options) => {
	const { bg, walls, size } = options;

	const container = new PIXI.Container();
	container.x = object.x;
	container.y = object.y;

	const containerBg = new PIXI.Container();
	const filter = new PIXI.filters.ColorMatrixFilter();
	filter.brightness(0.5);
	containerBg.filters = [filter];
	container.addChild(containerBg);

	const containerWalls = new PIXI.Container();
	container.addChild(containerWalls);

	const containerObjects = new PIXI.Container();
	container.addChild(containerObjects);

	for (let i = 0; i < size.w; i++) {
		for (let j = 0; j < size.h; j++) {
			const spriteBg = buildSprite((i * 8), (j * 8), bg.x, bg.y);
			containerBg.addChild(spriteBg);

			if (i === 0 || i === size.w - 1 || j === 0 || j === size.h - 1) {
				const spriteWall = buildSprite((i * 8), (j * 8), walls.x, walls.y);
				containerWalls.addChild(spriteWall);
			}
		}
	}

	const children = [];

	options.objects.forEach(t => {
		const childOptions = {
			x: 0,
			y: 0,
			container: containerObjects
		};

		const child = buildObject(t, childOptions);
		const childSprite = child.components.find(c => c.type === 'renderable')?.sprite;
		if (childSprite)
			containerObjects.addChild(childSprite);

		children.push(child);
	});

	return {
		type: 'room',
		container,
		template: options,
		children,
		update: () => {

		}
	};
};

export default cpn;
