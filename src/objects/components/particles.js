import * as particles from '@pixi/particle-emitter';

const cpn = (object, options) => {
	const container = object.container;

	const emitter = new particles.Emitter(
		container,
		options.buildBlueprint(options)
	);

	let elapsed = Date.now();

	emitter.emit = true;

	return {
		type: 'particles',

		update: () => {},

		render: () => {
			const now = Date.now();

			emitter.update((now - elapsed) * 0.001);
			elapsed = now;
		}
	};
};

export default cpn;
