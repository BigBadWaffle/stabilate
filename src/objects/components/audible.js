import { Howl } from 'howler';

import { waitForMeasure } from '../../system/music';

const cpn = (object, { sound, autoLoop, volume = 1, events }) => {
	let s = new Howl({
		src: [`audio/${sound}.ogg`],
		loop: autoLoop,
		volume
	});

	if (autoLoop)
		s.play();

	let waiting = false;
	let hasStarted = false;

	return {
		type: 'audible',

		update: () => {
			if (autoLoop)
				return;

			if (!waiting) {
				waiting = true;

				(
					async () => {
						await waitForMeasure();
						waiting = false;
						s.stop();
						s.play();

						if (!hasStarted) {
							hasStarted = true;

							if (events?.onStartPlaying)
								events.onStartPlaying(object);
						}
					}
				)();
			}
		}
	};
};

export default cpn;
