import * as PIXI from 'pixi.js';

import { getTexture } from '../../renderer/textureCache';

const cpn = (object, options) => {
	const sprite = new PIXI.Sprite(
		getTexture('images/tiles.png', options.spriteOffset.x, options.spriteOffset.y),
		{
			width: 8,
			height: 8
		}
	);
	sprite.x = options.x * 8;
	sprite.y = options.y * 8;

	object.container.addChild(sprite);

	return {
		type: 'renderable',
		sprite,
		update: () => {

		}
	};
};

export default cpn;
