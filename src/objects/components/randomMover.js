const cpn = object => {
	return {
		type: 'randomMover',
		update: () => {
			const dir = ~~(Math.random() * 3) - 1;
			object.x += dir * 8;

			if (object.x > (object.room.template.size.w - 2) * 8)
				object.x = (object.room.template.size.w - 2) * 8;
			else if (object.x < 8)
				object.x = 8;
		}
	};
};

export default cpn;
