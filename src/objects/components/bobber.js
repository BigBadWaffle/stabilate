const cpn = object => {
	let t = 0;

	const sprite = object.components.find(c => c.type === 'renderable').sprite;
	const initialY = sprite.y;
	const initialH = sprite.height;

	return {
		type: 'bobber',

		update: () => {},

		render: () => {
			t++;

			const delta = 1 * Math.sin(t / 32);

			sprite.height = initialH - delta;
			sprite.y = initialY + delta;
		}
	};
};

export default cpn;
