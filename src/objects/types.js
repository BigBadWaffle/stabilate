const types = [
	{
		type: 1,
		components: [
			{
				type: 'renderable',
				spriteOffset: {
					x: 0,
					y: 7
				}
			},
			{ type: 'randomMover' },
			{
				type: 'audible',
				sound: 'g_cs_1'
			}
		]
	},
	{
		type: 2,
		components: [
			{
				type: 'renderable',
				spriteOffset: {
					x: 1,
					y: 7
				}
			},
			{
				type: 'audible',
				sound: 'g_cs_2'
			}
		]
	},
	{
		type: 3,
		components: [
			{
				type: 'renderable',
				spriteOffset: {
					x: 1,
					y: 7
				}
			},
			{
				type: 'audible',
				sound: 'g_cs_3'
			}
		]
	},
	{
		type: 4,
		components: [
			{
				type: 'renderable',
				spriteOffset: {
					x: 1,
					y: 7
				}
			},
			{
				type: 'audible',
				sound: 'g_b_1'
			}
		]
	}
];

export default types;
