const objects = [];
let lastId = 0;

//Components
import room from './components/room';
import bobber from './components/bobber';
import audible from './components/audible';
import particles from './components/particles';
import renderable from './components/renderable';
import randomMover from './components/randomMover';

const typeMap = {
	room,
	bobber,
	audible,
	particles,
	renderable,
	randomMover
};

const buildComponent = (object, blueprint) => {
	const type = blueprint.type;

	return typeMap[type](object, blueprint);
};

export const buildObject = (template, options) => {
	//const x = template.x * 8;
	const object = {
		id: ++lastId,
		x: options.x,
		y: options.y,
		container: options.container,
		components: [],
		update: () => {
			object.components.forEach(c => c.update());
		},
		render: () => {
			object.components.forEach(c => {
				if (c.render)
					c.render();
			});
		},
		addComponent: config => object.components.push(buildComponent(object, config))
	};

	template.components.forEach(c => {
		object.components.push(buildComponent(object, c));
	});

	objects.push(object);

	return object;
};

export const update = () => {
	objects.forEach(o => {
		o.update();
	});
};

export const render = () => {
	objects.forEach(o => {
		o.render();
	});
};

export const init = () => {
	setInterval(update, 350);
};
