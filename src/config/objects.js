import * as PIXI from 'pixi.js';

const config = [
	{
		uiName: 'Rain Room',
		id: 'room-1',
		subType: 'room',
		components: [
			{
				type: 'room',
				bg: {
					x: 4,
					y: 1
				},
				walls: {
					x: 4,
					y: 0
				},
				size: {
					w: 11,
					h: 9
				},
				objects: [
					{
						x: 0,
						y: 0,
						components: [
							{
								type: 'particles',
								x: 1,
								y: 1,
								w: 9,
								h: 0.1,
								buildBlueprint: v => ({
									lifetime: {
										min: 1.2,
										max: 1.2
									},
									frequency: 0.005,
									spawnChance: 1,
									maxParticles: 1000,
									pos: {
										x: v.x * 8,
										y: v.y * 8
									},
									addAtBack: false,
									behaviors: [
										{
											type: 'alpha',
											config: {
												alpha: {
													list: [
														{
															value: 0,
															time: 0
														},
														{
															value: 1,
															time: 0.2
														},
														{
															value: 1,
															time: 0.8
														},
														{
															value: 0,
															time: 1
														}
													]
												}
											}
										},
										{
											type: 'scale',
											config: {
												scale: {
													list: [
														{
															value: 0.5,
															time: 0
														},
														{
															value: 0.49,
															time: 1
														}
													]
												}
											}
										},
										{
											type: 'color',
											config: {
												color: {
													list: [
														{
															value: '00aaff',
															time: 0
														},
														{
															value: '22bbff',
															time: 1
														}
													]
												}
											}
										},
										{
											type: 'movePath',
											config: {
												path: x => x,
												speed: {
													list: [{
														value: 5,
														time: 0
													}, {
														value: 30,
														time: 0.4
													}, {
														value: 60,
														time: 0.95
													}, {
														value: 0,
														time: 1
													}]
												},
												minMult: 0.8
											}
										},
										{
											type: 'rotationStatic',
											config: {
												min: 43,
												max: 47
											}
										},
										{
											type: 'spawnShape',
											config: {
												type: 'rect',
												data: {
													x: 0,
													y: 0,
													w: v.w * 8,
													h: v.h * 8
												}
											}
										},
										{
											type: 'textureSingle',
											config: { texture: PIXI.Texture.from('images/particles.png') }
										}
									]
								})
							}
						]
					}
				]
			},
			{
				type: 'audible',
				sound: 'rain',
				autoLoop: true,
				volume: 0.9
			}
		]
	},
	{
		uiName: 'Wind Room',
		id: 'room-4',
		subType: 'room',
		components: [
			{
				type: 'room',
				bg: {
					x: 0,
					y: 1
				},
				walls: {
					x: 0,
					y: 0
				},
				size: {
					w: 15,
					h: 6
				},
				objects: [
					{
						x: 0,
						y: 0,
						components: [
							{
								type: 'particles',
								x: 1,
								y: 1,
								w: 0.1,
								h: 4,
								buildBlueprint: v => ({
									lifetime: {
										min: 2.2,
										max: 2.2
									},
									frequency: 0.02,
									spawnChance: 1,
									maxParticles: 400,
									pos: {
										x: v.x * 8,
										y: v.y * 8
									},
									addAtBack: false,
									behaviors: [
										{
											type: 'alpha',
											config: {
												alpha: {
													list: [
														{
															value: 0,
															time: 0
														},
														{
															value: 1,
															time: 0.2
														},
														{
															value: 1,
															time: 0.6
														},
														{
															value: 0,
															time: 1
														}
													]
												}
											}
										},
										{
											type: 'scale',
											config: {
												scale: {
													list: [
														{
															value: 0.3,
															time: 0
														},
														{
															value: 0.29,
															time: 1
														}
													]
												}
											}
										},
										{
											type: 'color',
											config: {
												color: {
													list: [
														{
															value: 'ffffff',
															time: 0
														},
														{
															value: 'efefef',
															time: 1
														}
													]
												}
											}
										},
										{
											type: 'movePath',
											config: {
												path: x => x + (0.2 * Math.sin(x / 10)),
												speed: {
													list: [{
														value: 5,
														time: 0
													}, {
														value: 30,
														time: 0.4
													}, {
														value: 60,
														time: 0.95
													}, {
														value: 0,
														time: 1
													}]
												},
												minMult: 0.8
											}
										},
										{
											type: 'rotationStatic',
											config: {
												min: -40,
												max: -50
											}
										},
										{
											type: 'spawnShape',
											config: {
												type: 'rect',
												data: {
													x: 0,
													y: 0,
													w: v.w * 8,
													h: v.h * 8
												}
											}
										},
										{
											type: 'textureSingle',
											config: { texture: PIXI.Texture.from('images/particles.png') }
										}
									]
								})
							}
						]
					}
				]
			},
			{
				type: 'audible',
				sound: 'wind',
				autoLoop: true,
				volume: 0.55
			}
		]
	},
	{
		uiName: 'Fire Room',
		id: 'room-5',
		subType: 'room',
		components: [
			{
				type: 'room',
				bg: {
					x: 1,
					y: 1
				},
				walls: {
					x: 1,
					y: 0
				},
				size: {
					w: 7,
					h: 7
				},
				objects: [
					{
						x: 0,
						y: 0,
						components: [
							{
								type: 'particles',
								x: 3,
								y: 5.5,
								w: 1,
								h: 0.5,
								buildBlueprint: v => ({
									lifetime: {
										min: 1.2,
										max: 1.2
									},
									frequency: 0.002,
									spawnChance: 1,
									maxParticles: 1000,
									pos: {
										x: v.x * 8,
										y: v.y * 8
									},
									addAtBack: false,
									behaviors: [
										{
											type: 'alpha',
											config: {
												alpha: {
													list: [
														{
															value: 0,
															time: 0
														},
														{
															value: 1,
															time: 0.2
														},
														{
															value: 1,
															time: 0.6
														},
														{
															value: 0,
															time: 1
														}
													]
												}
											}
										},
										{
											type: 'scale',
											config: {
												scale: {
													list: [
														{
															value: 1,
															time: 0
														},
														{
															value: 0.2,
															time: 1
														}
													]
												}
											}
										},
										{
											type: 'color',
											config: {
												color: {
													list: [
														{
															value: 'ffaa00',
															time: 0
														},
														{
															value: 'aa2200',
															time: 1
														}
													]
												}
											}
										},
										{
											type: 'movePath',
											config: {
												speed: {
													list: [{
														value: 5,
														time: 0
													}, {
														value: 10,
														time: 0.4
													}, {
														value: 20,
														time: 0.95
													}, {
														value: 0,
														time: 1
													}]
												},
												minMult: 0.8
											}
										},
										{
											type: 'rotationStatic',
											config: {
												min: -180,
												max: -90
											}
										},
										{
											type: 'spawnShape',
											config: {
												type: 'rect',
												data: {
													x: 0,
													y: 0,
													w: v.w * 8,
													h: v.h * 8
												}
											}
										},
										{
											type: 'textureSingle',
											config: { texture: PIXI.Texture.from('images/particles.png') }
										}
									]
								})
							}
						]
					}
				]
			},
			{
				type: 'audible',
				sound: 'fire',
				autoLoop: true,
				volume: 0.3
			}
		]
	},
	{
		uiName: 'Acoustic Room 1',
		id: 'room-2',
		subType: 'room',
		components: [
			{
				type: 'room',
				bg: {
					x: 2,
					y: 1
				},
				walls: {
					x: 2,
					y: 0
				},
				size: {
					w: 5,
					h: 5
				},
				objects: [
					{
						components: [
							{
								x: 2,
								y: 3,
								type: 'renderable',
								spriteOffset: {
									x: 0,
									y: 7
								}
							}
						]
					}
				]
			},
			{
				type: 'audible',
				sound: 'g_cs_1',
				events: {
					onStartPlaying: v => {
						const actor = v.components.find(c => c.type === 'room').children[0];
						const hasBobber = actor.components.some(c => c.type === 'bobber');
						if (hasBobber)
							return;

						actor.addComponent({ type: 'bobber' });
					}
				}
			}
		]
	},
	{
		uiName: 'Acoustic Room 2',
		id: 'room-3',
		subType: 'room',
		components: [
			{
				type: 'room',
				bg: {
					x: 2,
					y: 1
				},
				walls: {
					x: 2,
					y: 0
				},
				size: {
					w: 5,
					h: 5
				},
				objects: [
					{
						components: [
							{
								x: 2,
								y: 3,
								type: 'renderable',
								spriteOffset: {
									x: 2,
									y: 7
								}
							}
						]
					}
				]
			},
			{
				type: 'audible',
				sound: 'g_cs_2',
				events: {
					onStartPlaying: v => {
						const actor = v.components.find(c => c.type === 'room').children[0];
						const hasBobber = actor.components.some(c => c.type === 'bobber');
						if (hasBobber)
							return;

						actor.addComponent({ type: 'bobber' });
					}
				}
			}
		]
	},
	{
		uiName: 'Bass Room 1',
		id: 'bass-room-3',
		subType: 'room',
		components: [
			{
				type: 'room',
				bg: {
					x: 2,
					y: 1
				},
				walls: {
					x: 2,
					y: 0
				},
				size: {
					w: 5,
					h: 5
				},
				objects: [
					{
						components: [
							{
								x: 2,
								y: 3,
								type: 'renderable',
								spriteOffset: {
									x: 1,
									y: 7
								}
							}
						]
					}
				]
			},
			{
				type: 'audible',
				sound: 'g_b_1',
				events: {
					onStartPlaying: v => {
						const actor = v.components.find(c => c.type === 'room').children[0];
						const hasBobber = actor.components.some(c => c.type === 'bobber');
						if (hasBobber)
							return;

						actor.addComponent({ type: 'bobber' });
					}
				}
			}
		]
	}
];

export default config;
