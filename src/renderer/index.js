//Plugins
import * as PIXI from 'pixi.js';

//System
import consts from '../system/consts';
import { getGlobal } from '../system/globals';

//Helpers
import { buildObject, render as renderObjects } from '../objects';
import templates from '../config/objects';

const rooms = [];

let renderer = null;
let stage = null;

//Helpers
const getRoomPosition = (template, { clientX }) => {
	const clickX = (~~(clientX / 40) * 40) / 5;

	const intersectX = rooms.filter(r => {
		const rTemplate = r.components.find(c => c.type === 'room')?.template;
		if (!rTemplate)
			return;

		return (!(clickX + (template.size.w * 8) < r.x) && (clickX < r.x + (rTemplate.size.w * 8)));
	});

	const x = clickX;
	let y = consts.floor.y - (template.size.h * 8);

	intersectX.forEach(({ y: roomY }) => {
		if (y + (template.size.h * 8) >= roomY)
			y = roomY - (template.size.h * 8);
	});

	return {
		x,
		y
	};
};

//Events
const setup = () => {
	PIXI.settings.GC_MODE = PIXI.GC_MODES.AUTO;
	PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;
	PIXI.settings.SPRITE_MAX_TEXTURES = Math.min(PIXI.settings.SPRITE_MAX_TEXTURES, 16);
	PIXI.settings.RESOLUTION = 1;

	renderer = new PIXI.Renderer({
		width: window.innerWidth,
		height: window.innerHeight,
		backgroundColor: '0x2d2136'
	});

	document.getElementById('canvas-container').appendChild(renderer.view);

	stage = new PIXI.Container();

	stage.scale.x = 5;
	stage.scale.y = 5;

	let baseTex = PIXI.Texture.from('images/tiles.png');
	let texture = new PIXI.Texture(baseTex, new PIXI.Rectangle(0, 0, 8, 8));

	let sprite = PIXI.TilingSprite.from(texture, {
		width: consts.world.w,
		height: 8
	});
	sprite.x = 0;
	sprite.y = consts.floor.y;

	stage.addChild(sprite);

	return [renderer, stage];
};

const onCreateRoom = e => {
	const roomTemplate = getGlobal('roomPicker').chosenRoom;

	const template = templates.find(t => t.id === roomTemplate);
	const { x, y } = getRoomPosition(template.components.find(c => c.type === 'room'), e);
	const options = {
		x,
		y
	};

	const room = buildObject(template, options);
	const roomContainer = room.components.find(c => c.type === 'room').container;

	rooms.push(room);

	stage.addChild(roomContainer);
};

export const addChild = child => {
	stage.addChild(child);
};

const render = () => {
	renderObjects();
	renderer.render(stage);
	requestAnimationFrame(render.bind(null, renderer, stage));
};

export const init = () => {
	setup();

	render();

	document.getElementById('canvas-container').children[0].addEventListener('mousedown', e => {
		onCreateRoom(e);
	});

	document.getElementById('canvas-container').children[0].addEventListener('touchend', e => {
		onCreateRoom(e);
	});
};
