import consts from '../system/consts';

import { getTexture } from './textureCache';

import { buildObject } from '../objects';

import templates from '../config/objects';

import * as PIXI from 'pixi.js';

const buildSprite = (x, y, sx, sy) => {
	const texture = getTexture('images/tiles.png', sx, sy);

	let sprite = new PIXI.Sprite(texture, {
		width: 8,
		height: 8
	});
	sprite.x = x;
	sprite.y = y;

	return sprite;
};

const rooms = [];
let lastId = 0;

const getRoomPosition = (template, { clientX }) => {
	const clickX = (~~(clientX / 40) * 40) / 5;

	const intersectX = rooms.filter(r => {
		const rTemplate = r.components.find(c => c.type === 'room')?.template;
		if (!rTemplate)
			return;

		return (!(clickX + (template.size.w * 8) < r.x) && (clickX < r.x + (rTemplate.size.w * 8)));
	});

	const x = clickX;
	let y = consts.floor.y - (template.size.h * 8);

	intersectX.forEach(({ y: roomY }) => {
		if (y + (template.size.h * 8) >= roomY)
			y = roomY - (template.size.h * 8);
	});

	return {
		x,
		y
	};
};

const buildRoom = (templateType, e) => {
	const template = templates.find(({ type }) => type === templateType);
	const { bg, walls, size } = template;

	const { x, y } = getRoomPosition(template, e);

	const container = new PIXI.Container();
	container.x = x;
	container.y = y;

	const containerBg = new PIXI.Container();
	const filter = new PIXI.filters.ColorMatrixFilter();
	filter.brightness(0.5);
	containerBg.filters = [filter];
	container.addChild(containerBg);

	const containerWalls = new PIXI.Container();
	container.addChild(containerWalls);

	const containerObjects = new PIXI.Container();
	container.addChild(containerObjects);

	for (let i = 0; i < size.w; i++) {
		for (let j = 0; j < size.h; j++) {
			const spriteBg = buildSprite((i * 8), (j * 8), bg.x, bg.y);
			containerBg.addChild(spriteBg);

			if (i === 0 || i === size.w - 1 || j === 0 || j === size.h - 1) {
				const spriteWall = buildSprite((i * 8), (j * 8), walls.x, walls.y);
				containerWalls.addChild(spriteWall);
			}
		}
	}

	const room = {
		id: ++lastId,
		template,
		container,
		containerObjects,
		objects: [],
		x,
		y
	};

	rooms.push(room);

	template.objects.forEach(t => {
		room.objects.push(buildObject({
			room,
			template: t
		}));
	});

	return container;
};

export default buildRoom;
