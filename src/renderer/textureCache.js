import * as PIXI from 'pixi.js';

const textureCache = [];

export const getTexture = (sheet, x, y) => {
	const cached = textureCache.find(c => c.sheet === sheet && c.x === x && c.y === y);
	if (cached)
		return cached.texture;

	let baseTextureEntry = textureCache.find(c => c.sheet === sheet && c.base);
	if (!baseTextureEntry) {
		baseTextureEntry = {
			sheet,
			base: true,
			texture: PIXI.Texture.from(sheet)
		};
		baseTextureEntry.texture.scaleMode = PIXI.SCALE_MODES.NEAREST;
		textureCache.push(baseTextureEntry);
	}

	let texture = new PIXI.Texture(baseTextureEntry.texture, new PIXI.Rectangle(x * 8, y * 8, 8, 8));
	texture.scaleMode = PIXI.SCALE_MODES.NEAREST;
	textureCache.push({
		sheet,
		x,
		y,
		texture
	});

	return texture;
};
