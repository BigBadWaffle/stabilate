const measureDurationMs = 11294;

const waiting = [];

let nextMeasure = 0;
let started = false;

const update = () => {
	const time = +new Date();

	if (time >= nextMeasure) {
		nextMeasure = nextMeasure += measureDurationMs;

		waiting.forEach(w => w());
		waiting.length = 0;
	}

	setTimeout(update, 10);
};

export const waitForMeasure = async () => {
	return new Promise(res => {
		if (!started) {
			started = true;
			nextMeasure = +new Date() + measureDurationMs;

			res();

			return;
		}

		waiting.push(res);
	});
};

export const init = () => {
	requestAnimationFrame(update);
};
