const globals = { roomPicker: { chosenRoom: 'room-1' } };

const setGlobal = (key, value) => {
	globals[key] = value;
};

const getGlobal = key => {
	return globals[key];
};

export {
	setGlobal,
	getGlobal
};
