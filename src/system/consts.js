const consts = {
	world: {
		w: window.innerWidth / 5,
		h: window.innerHeight / 5
	},
	floor: {
		y: (window.innerHeight / 5) - 8,
		h: 8
	},
	physicsOffset: {
		x: 500,
		y: 500
	}
};

export default consts;
