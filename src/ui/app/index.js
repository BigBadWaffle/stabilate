//Ui
import RoomPicker from '../roomPicker';

import './styles.css';

const App = () => {
	return (
		<div className='uiApp'>
			<RoomPicker />
		</div>
	);
};

export default App;
