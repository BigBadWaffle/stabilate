//React
import { useState } from 'react';

//System
import { setGlobal } from '../../system/globals';

//Styles
import './styles.css';

//Config
import objectConfig from '../../config/objects';

//Events
const onChooseRoom = (state, setState, chosenRoom) => {
	state.chosenRoom = chosenRoom;

	setState({ chosenRoom });
	setGlobal('roomPicker', state);
};

//Components
const RoomTypes = ({ state, setState }) => {
	const result = objectConfig.map(o => {
		const key = `option-${o.id}`;
		const selected = state.chosenRoom === o.id;

		const className = selected ? 'selected' : '';

		return (
			<div
				key={key}
				className={className}
				onClick={onChooseRoom.bind(null, state, setState, o.id)}
			>
				{o.uiName}
			</div>
		);
	});

	return result;
};

const RoomPicker = () => {
	const [state, setState] = useState({ chosenRoom: 'room-1' });

	return (
		<div className='uiRoomPicker'>
			<RoomTypes state={state} setState={setState} />
		</div>
	);
};

export default RoomPicker;
